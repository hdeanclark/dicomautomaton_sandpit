//Tray_Notification.cc - A part of DICOMautomaton 2021. Written by hal clark.

#include <algorithm>
#include <array>
#include <cmath>
#include <cstdint>
#include <cctype>
#include <cstdlib>            //Needed for exit() calls.
#include <cstdio>
#include <cstddef>
#include <exception>
#include <any>
#include <optional>
#include <fstream>
#include <functional>
#include <filesystem>
#include <iostream>
#include <iterator>
#include <limits>
#include <list>
#include <map>
#include <set>
#include <memory>
#include <numeric>
#include <regex>
#include <stdexcept>
#include <string>    
#include <tuple>
#include <type_traits>
#include <utility>            //Needed for std::pair.
#include <vector>
#include <chrono>
#include <future>
#include <mutex>
#include <shared_mutex>
#include <initializer_list>
#include <thread>

#include "YgorMath.h"         //Needed for vec3 class.
#include "YgorMisc.h"         //Needed for FUNCINFO, FUNCWARN, FUNCERR macros.
#include "YgorString.h"       //Needed for GetFirstRegex(...)

#include "../Thread_Pool.h"

#include "Tray_Notification.h"

static
void
array_to_string(std::string &s, const std::array<char, 1024> &a){
    s.clear();
    for(const auto &c : a){
        if(c == '\0') break;
        s.push_back(c);
    }
    return;
}

static
void
string_to_array(std::array<char, 1024> &a, const std::string &s){
    a.fill('\0');
    for(size_t i = 0; (i < s.size()) && ((i+1) < a.size()); ++i){
        a[i] = s[i];
        //a[i+1] = '\0';
    }
    return;
}

// Remove characters so that the argument can be inserted like '...' on command line.
static
std::string
escape_for_quotes(std::string s){
    // Remove unprintable characters and newlines.
    const auto rem = [](unsigned char c){
        return (   !std::isprint(c)
                || (c == '\n')
                || (c == '\r') );
    };
    s.erase( std::remove_if(std::begin(s), std::end(s), rem),
             std::end(s) );

    // Replace all single quotes with double quotes.
    for(auto &c : s) if(c == '\'') c = '"';

    return s;
}


bool
tray_notification(const notification_t &n){

    enum class query_method {
        notifysend,
        zenity,
        pshell,
    };
    std::set<query_method> qm;
#if defined(_WIN32) || defined(_WIN64)
    qm.insert( query_method::pshell );
#endif
#if defined(__linux__)
    qm.insert( query_method::notifysend );
    qm.insert( query_method::zenity );
#endif

    long int tries = 0;
    while(tries++ < 3){
        try{

            // Prepare the query parameters.
            std::map<std::string, std::string> key_vals;
            key_vals["TITLE"] = escape_for_quotes("DICOMautomaton");
            key_vals["MESSAGE"] = escape_for_quotes(n.message);

/*
powershell -Command "& { [void] [System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms'); $objNotifyIcon=New-Object System.Windows.Forms.NotifyIcon; $objNotifyIcon.BalloonTipText='This is a test.'; $objNotifyIcon.Icon=[system.drawing.systemicons]::Information; $objNotifyIcon.BalloonTipTitle='DICOMautomaton'; $objNotifyIcon.BalloonTipIcon='None'; $objNotifyIcon.Visible=$True; $objNotifyIcon.ShowBalloonTip(10000); }"

            // Windows Powershell (VisualBasic).
            if(qm.count(query_method::pshell) != 0){

                // Build the invocation.
                const std::string proto_cmd = R"***(powershell -Command " & {Add-Type -AssemblyName Microsoft.VisualBasic; [Microsoft.VisualBasic.Interaction]::InputBox('$QUERY', '$TITLE', '$DEFAULT')}")***";
                std::string cmd = ExpandMacros(proto_cmd, key_vals, "$");

                // Query the user.
                FUNCINFO("About to perform pshell command: '" << cmd << "'");
                auto res = Execute_Command_In_Pipe(cmd);
                res = escape_for_quotes(res); // Trim newlines and unprintable characters.
                FUNCINFO("Received user input: '" << res << "'");

                // Ensure the input fits the required data type.
                if(uq.val_type == user_input_t::string){
                    uq.val = res;
                }else if(uq.val_type == user_input_t::real){
                    uq.val = std::stod(res);
                }else if(uq.val_type == user_input_t::integer){
                    uq.val = static_cast<int64_t>(std::stoll(res));
                }

                // Break out of the while loop on success.
                uq.answered = true;
                break;
            }
*/

            // Notify-send.
            if(qm.count(query_method::notifysend) != 0){

                // Build the invocation.
                const std::string proto_cmd = R"***(notify-send --app-name='DICOMautomaton' --urgency='normal' --expire-time='10000' '@TITLE' '@MESSAGE' || echo notavailable)***";
                std::string cmd = ExpandMacros(proto_cmd, key_vals, "@");

                // Notify the user.
                FUNCINFO("About to perform notify-send command: '" << cmd << "'");
                auto res = Execute_Command_In_Pipe(cmd);
                res = escape_for_quotes(res); // Trim newlines and unprintable characters.

                if(res != "notavailable") break;
            }

/*
            // Zenity.
            if(qm.count(query_method::zenity) != 0){

                // Build the invocation.
                const std::string proto_cmd = R"***(zenity --title='@TITLE' --entry --text='@QUERY' --entry-text='@DEFAULT')***";
                std::string cmd = ExpandMacros(proto_cmd, key_vals, "@");

                // Query the user.
                FUNCINFO("About to perform zenity command: '" << cmd << "'");
                auto res = Execute_Command_In_Pipe(cmd);
                res = escape_for_quotes(res); // Trim newlines and unprintable characters.
                FUNCINFO("Received user input: '" << res << "'");

                // Ensure the input fits the required data type.
                if(uq.val_type == user_input_t::string){
                    uq.val = res;
                }else if(uq.val_type == user_input_t::real){
                    uq.val = std::stod(res);
                }else if(uq.val_type == user_input_t::integer){
                    uq.val = static_cast<int64_t>(std::stoll(res));
                }

                // Break out of the while loop on success.
                uq.answered = true;
                break;
            }
*/
        }catch(const std::exception &e){
            FUNCWARN("User notification failed: '" << e.what() << "'");
        }
    }

    return (tries < 3);
}

