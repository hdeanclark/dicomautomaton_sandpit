#!/bin/bash

notify-send --app-name='DICOMautomaton' --urgency='normal' --expire-time='10000' 'SDL Viewer' 'A core component of the SDL Viewer is currently on fire.'

